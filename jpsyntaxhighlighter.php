<?php
/**
 * @package     Joomlaportal.Plugins
 * @subpackage  Content.JPSyntaxHighlighter
 *
 * @author      Joomlaportal.ru Team <smart@joomlaportal.ru>
 * @copyright   Copyright (C) 2013-2014 Joomlaportal.ru. All rights reserved.
 * @license     GNU General Public License version 3 or later; see license.txt
 */

defined('JPATH_BASE') or die;

/**
 * The Joomlaportal.ru SyntaxHighlighter plugin
 *
 * @package     Joomlaportal.Plugins
 * @subpackage  Content.JPSyntaxHighlighter
 */
class plgContentJPSyntaxHighlighter extends JPlugin
{
	private static $themes = array(
		'default' => 'shThemeDefault.css',
		'django' => 'shThemeDjango.css',
		'emacs' => 'shThemeEmacs.css',
		'fadetogrey' => 'shThemeFadeToGrey.css',
		'midnight' => 'shThemeMidnight.css',
		'rdark' => 'shThemeRDark.css'
	);

	private static $brushes = array(
		'css' => 'shBrushCss.js',
		'diff' => 'shBrushDiff.js',
		'javascript' => 'shBrushJScript.js',
		'php' => 'shBrushPhp.js',
		'text' => 'shBrushPlain.js',
		'sql' => 'shBrushSql.js',
		'html' => 'shBrushXml.js',
		'xml' => 'shBrushXml.js'
	);

	private static $initialized = false;
	private static $loaded = array();

	/**
	 * Plugin uses SyntaxHighlighter for highlighting source code inside PRE tags
	 *
	 * Syntax: <pre xml:lang="language">code</pre>
	 *
	 * @param   string  $context The context of the content being passed to the plugin.
	 * @param   object  &$row    The article object.  Note $article->text is also available
	 * @param   mixed   &$params The article params
	 * @param   integer $page    The 'page' number
	 *
	 * @return  mixed  Always returns void or true
	 */
	function onContentPrepare($context, &$row, &$params, $page = 0)
	{
		$regex = "#<pre[\s|&nbsp;]*(.*?)>([\s\S]*?)</pre>#s";
		$row->text = preg_replace_callback($regex, array($this, 'replace'), $row->text);

		return true;
	}

	/**
	 * Wraps source code with necessary tags to use with SyntaxHighlighter
	 *
	 * @param array $matches An array of matches (see preg_match_all)
	 *
	 * @return string
	 */
	private function replace($matches)
	{
		if (!self::$initialized)
		{
			$this->loadSyntaxHighlighter();
			self::$initialized = true;
		}

		$html_entities_match = array("'\n'", "'\<br\s*\/*\s*\>'i", "'<'i", "'>'i", "'&#39;'", "'&quot;'i", "'&nbsp;'i");
		$html_entities_replace = array("", "\n", '&lt;', '&gt;', "'", '"', ' ');

		$text = preg_replace($html_entities_match, $html_entities_replace, $matches[2]);
		$options = $matches[1];

		$language = $this->params->get('default-language');
                $width = $this->params->def('width', '500');

		if (!empty($options)) {
			$count = preg_match_all('#xml\:lang\=\"([a-z]+)\"#i', $options, $m);
			if ($count > 0) {
				$language = strtolower($m[1][0]);
			}
                        $count = preg_match_all('#width\=\"([\d]+(px|%)?)\"#i', $options, $m);
                        if ($count > 0) {
				$width = $m[1][0];
			}
		}
                $width = str_replace('px', '', $width);
                if (strrpos($width, '%') == false) {
                    $width .= 'px';
                }

		$this->loadSyntaxHighlighterBrush($language);
		
		$class = 'brush:' . $language;

		$text = '<div style="overflow: hidden; display: block; height: auto; width: ' . $width . ';">'
			. '<pre class="' . $class . '">'
			. $text
			. '</pre></div>';

		return $text;
	}

	/**
	 * Loads SyntaxHighlighter's brush for given language
	 *
	 * @param   string $language The language name
	 */
	private function loadSyntaxHighlighterBrush($language)
	{
		if (!isset(self::$loaded[$language]))
		{
			if (isset(self::$brushes[$language]))
			{
				JHtml::_('script', 'plg_' . $this->_type . '_' . $this->_name . '/' . self::$brushes[$language], false, true, false);
			}
			self::$loaded[$language] = true;
		}
	}

	/**
	 * Loads Core and Theme CSS for SyntaxHighlighter and initialises SyntaxHighlighter's parameters
	 */
	private function loadSyntaxHighlighter()
	{
		$theme = strtolower($this->params->def('theme', 'default'));
		$stylesheet = isset(self::$themes[$theme]) ? self::$themes[$theme] : self::$themes['default'];
		$pluginName = 'plg_' . $this->_type . '_' . $this->_name;

		JHtml::_('stylesheet', $pluginName . '/shCore.css', false, true, false);
		JHtml::_('stylesheet', $pluginName . '/' . $stylesheet, false, true, false);

		JHtml::_('script', $pluginName . '/shCore.js', false, true, false);

		$script = 'SyntaxHighlighter.config.clipboardSwf = "' . JURI::root() . 'media/' . $pluginName . '/js/clipboard.swf";' . "\n"
			. 'SyntaxHighlighter.defaults["auto-links"] = ' . ($this->params->def('auto-links', 0) ? 'true' : 'false') . ";\n"
			. 'SyntaxHighlighter.defaults["collapse"] = ' . ($this->params->def('collapse', 0) ? 'true' : 'false') . ";\n"
			. 'SyntaxHighlighter.defaults["gutter"] = ' . ($this->params->def('gutter', 0) ? 'true' : 'false') . "\n"
			. 'SyntaxHighlighter.defaults["smart-tabs"] = ' . ($this->params->def('smart-tabs', 0) ? 'true' : 'false') . ";\n"
			. 'SyntaxHighlighter.defaults["tab-size"] = ' . $this->params->def('tab-size', '4') . ";\n"
			. 'SyntaxHighlighter.defaults["toolbar"] = ' . ($this->params->def('toolbar', 0) ? 'true' : 'false') . ";\n"
			. 'SyntaxHighlighter.defaults["wrap-lines"] = ' . ($this->params->def('wrap-lines', 0) ? 'true' : 'false') . ";\n"
			. 'SyntaxHighlighter.all();' . "\n";

		JFactory::getDocument()->addScriptDeclaration($script);

		return;
	}
}
